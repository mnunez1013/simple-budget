from pydantic import BaseModel

class ExpenseIn(BaseModel):
    name: str
    cost: int
    due_date: str
    user_id: int

class ExpenseOut(BaseModel):
    id: int
    name: str
    cost: int
    due_date: str
    user_id: int

class DeleteStatus(BaseModel):
    status: bool