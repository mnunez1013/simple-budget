from pydantic import BaseModel

class UserIn(BaseModel):
    username: str
    password: str
    current_money: int
    due_date: str

class UserOut(BaseModel):
    id: int
    username: str
    password: str
    current_money: int
    due_date: str

class DeleteStatus(BaseModel):
    status: bool