from models.expenses import ExpenseIn, ExpenseOut

class ExpenseQueries:
    def get_expense(self, user_id: int, expense_id: int):
        return {
            "id": expense_id,
            "name": 'rent',
            "cost": 1300,
            "due_date": 1,
            "user_id": user_id
        }

    def get_expenses(self, user_id: int):
        return [
            {
                "id": 5,
                "name": 'verizon',
                "cost": 55,
                "due_date": 15,
                "user_id": user_id
            },
            {
                "id": 5,
                "name": 'electric',
                "cost": 100,
                "due_date": 17,
                "user_id": user_id
            }
        ]

    def create_expense(self, user_id: int, expense: ExpenseIn):
        return {
            "id": 5,
            "name": 'createdExpense',
            "cost": 25,
            "due_date": 22,
            "user_id": user_id
        }

    def update_expense(self, user_id: int, expense_id: int, expense: ExpenseIn):
        return {
            "id": 5,
            "name": 'createdExpense',
            "cost": 25,
            "due_date": 22,
            "user_id": user_id
        }

    def delete_expenses(self, user_id: int):
        return {
            "status": True
        }

    def delete_expense(self, user_id: int, expense_id: int):
        return {
            "status": True
        }