from models.users import UserIn

class UserQueries:
    def get_user(self, user_id: int):
        return {
            "id": user_id,
            "username": 'test',
            "password": 'test',
            "current_money": 10,
            "due_date": 'Friday'
        }
    
    def create_user(self, user: UserIn):
        return {
            "id": 5,
            "username": 'Michael',
            "password": "supersecure",
            "current_money": 25,
            "due_date": "Saturday"
        }
    
    def update_user(self, user_id: int, user: UserIn):
        return {
            "id": 20,
            "username": "New New",
            "password": "new Password",
            "current_money": 50,
            "due_date": "New Date"
        }
    
    def delete_user(self, user_id: int):
        return {
            "status": True
        }