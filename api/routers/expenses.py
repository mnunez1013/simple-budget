from fastapi import APIRouter, Depends
from models.expenses import ExpenseIn, ExpenseOut, DeleteStatus
from typing import List
from queries.expenses import ExpenseQueries

router = APIRouter()

@router.get('/v1/users/{user_id}/expenses', response_model=List[ExpenseOut])
def get_all_expenses(user_id: int, query: ExpenseQueries = Depends()):
    return query.get_expenses(user_id)

@router.get('/v1/users/{user_id}/expenses/{expense_id}', response_model=ExpenseOut)
def get_expense(user_id: int, expense_id: int, query: ExpenseQueries = Depends()):
    return query.get_expense(user_id, expense_id)

@router.post('/v1/users/{user_id}/expenses', response_model=ExpenseOut)
def create_expense(user_id: int, expense: ExpenseIn, query: ExpenseQueries = Depends()):
    return query.create_expense(user_id, expense)

@router.put('/v1/users/{user_id}/expenses/{expense_id}', response_model=ExpenseOut)
def update_expense(user_id: int, expense_id: int, expense: ExpenseIn, query: ExpenseQueries = Depends()):
    return query.update_expense(user_id, expense_id, expense)

@router.delete('/v1/users/{user_id}/expenses', response_model=DeleteStatus)
def delete_all_expenses(user_id: int, query: ExpenseQueries = Depends()):
    return query.delete_expenses(user_id)

@router.delete('/v1/users/{user_id}/expenses/{expense_id}', response_model=DeleteStatus)
def delete_expense(user_id: int, expense_id: int, query: ExpenseQueries = Depends()):
    return query.delete_expense(user_id, expense_id)