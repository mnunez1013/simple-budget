from fastapi import APIRouter, Depends
from models.users import UserOut, UserIn, DeleteStatus
from queries.users import UserQueries

router = APIRouter()

@router.get('/v1/users/{user_id}', response_model=UserOut)
def get_user(user_id: int, query: UserQueries = Depends()):
    return query.get_user(user_id)

@router.post('/v1/users', response_model=UserOut)
def create_user(user: UserIn, query: UserQueries = Depends()):
    return query.create_user(user)

@router.put('/v1/users/{user_id}', response_model=UserOut)
def update_user(user_id: int, new_user_data: UserIn, query: UserQueries = Depends()):
    return query.update_user(user_id, new_user_data)

@router.delete('/v1/users/{user_id}', response_model=DeleteStatus)
def delete_user(user_id: int, query: UserQueries = Depends()):
    return query.delete_user(user_id)